import {
  Box,
  Flex,
  Icon,
  Text,
  useColorModeValue as mode,
} from '@chakra-ui/react';

const DesktopNavLink = ({ active, children, ...rest }: any) => {
  return (
    <Text
      aria-current={active ? 'page' : undefined}
      fontWeight="semibold"
      color={mode('gray.300', 'gray.500')}
      cursor="pointer"
      {...rest}
      _activeLink={{
        color: mode('gray.600', 'gray.300'),
      }}
      _hover={{
        color: mode('gray.600', 'gray.300'),
      }}
    >
      {children}
    </Text>
  );
};

interface MobileNavLinkProps {
  icon: React.ElementType;
  children: React.ReactNode;
  active?: boolean;
  onClick?: any;
}

const MobileNavLink = ({
  icon,
  children,
  active,
  onClick,
}: MobileNavLinkProps) => {
  return (
    <Flex
      as="a"
      onClick={onClick}
      m="-3"
      p="3"
      align="center"
      rounded="md"
      cursor="pointer"
      _hover={{ bg: mode('gray.50', 'gray.600') }}

      // bgColor={{active && mode('gray.600', 'gray.300')}}
      // _active={
      //   active && {
      //     ,
      //   }
      // }
    >
      <Icon as={icon} color={mode('gray.600', 'gray.400')} fontSize="xl" />
      <Box marginStart="3" fontWeight="medium">
        {children}
      </Box>
    </Flex>
  );
};

export const NavLink = {
  Desktop: DesktopNavLink,
  Mobile: MobileNavLink,
};
