import { hex2bin } from '../../utils';

const PIXELS_PER_EDGE = 2;

export function BinaryTreeBranch({ binaryTreeData, isHighlighted }: { binaryTreeData: BinaryTreeData | null, isHighlighted: boolean }) {
    if (!binaryTreeData) {
        return null;
    }
    const points = binaryTreeData.points;
    const viewBoxSize = (points.length - 1) * PIXELS_PER_EDGE;
    return <svg viewBox={`0 0 ${viewBoxSize} ${viewBoxSize}`} xmlns="http://www.w3.org/2000/svg">
        <polyline points={points.map(coord => `${coord[0]},${coord[1]}`).join(' ')}
            fill="none" stroke="black" strokeWidth={isHighlighted ? 2 : 1} />
    </svg>;
}

export function getBinaryTreeData(hash: string | null): BinaryTreeData | null {
    if (!hash) {
        return null
    }
    const binaryData = hex2bin(hash);
    const numberOfPoints = binaryData.length;
    const points = binaryData.split('').reduce((prev, curr) => {
        const [x, y] = prev[prev.length - 1];
        const newX = curr === '0' ? x - PIXELS_PER_EDGE : x + PIXELS_PER_EDGE;
        return prev.concat([[newX, y + PIXELS_PER_EDGE]]);
    }, [[numberOfPoints, 0]] as Array<[number, number]>);

    const xCoords = points.map(coord => coord[0]);
    const minXCoord = Math.min(...xCoords);
    const maxXCoord = Math.max(...xCoords);
    const maxDrift = Math.max(numberOfPoints - minXCoord, maxXCoord - numberOfPoints) / PIXELS_PER_EDGE;

    return {
        points: points,
        maxDrift: maxDrift
    }
}