import { Box, Select } from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { ChangeEvent, useEffect, useState } from 'react';
import Highlighter from "react-highlight-words";

import { useSelectedHash } from '../../context';
import HASHES_ABI from '../../Hashes.json';
import { useContract, useDeviceDetect } from '../../hooks';
import { HASHES_ADDRESS, hex2bin } from '../../utils';

const SAMPLE_HASH = '0x1c39ae79a25c803c3620cfae1dd77ce1a10655e3348d6dbeafae1702a809837f';

function truncate(str, n) {
  return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
};

export default function HashBinarySelector({
  activeRegex,
  highlightFunction
}: {
  activeRegex: RegExp | undefined;
  highlightFunction: HighlightFunction | undefined
}) {
  const [selectedHash, setSelectedHash] = useSelectedHash();
  const { isMobile } = useDeviceDetect();

  const { account, active, chainId } = useWeb3React();
  const HashesContract = useContract(
    chainId && HASHES_ADDRESS[chainId],
    HASHES_ABI.abi,
    true,
  );
  const [hashes, setHashes] = useState<any[]>([]);

  useEffect(() => {
    (async () => {
      if (HashesContract && account) {
        try {
          const balance = await HashesContract.balanceOf(account);

          const hashes: any[] = [];
          for (let i = 0; i < balance.toNumber(); i++) {
            const tokenId = await HashesContract.tokenOfOwnerByIndex(
              account,
              i,
            );
            const [hash, response] = await Promise.all([
              HashesContract.getHash(tokenId),
              fetch(`/api/token/${tokenId}`)
            ]);
            const { attributes } = await response.json();
            let phrase = 'unknown';
            if (Array.isArray(attributes)) {
              const phraseAttribute = attributes.find(attribute => attribute.trait_type === "Phrase");
              if (phraseAttribute) {
                phrase = truncate(phraseAttribute.value, 24);
              }
            }
            hashes.push({ hash, phrase });
          }

          if (hashes.length === 0) hashes.push({ hash: SAMPLE_HASH, phrase: 'Example' });

          setHashes(hashes);
        } catch (error: any) {
          console.error(error);
        }
      }
    })();
  }, [account, HashesContract]);

  if (!hashes) {
    return null;
  }

  const binaryData = hex2bin(selectedHash);

  return (
    <Box position="relative">
      <Box
        position={isMobile ? "inherit" : "absolute"}
        top={isMobile ? "inherit" : "11vh"}
        left={isMobile ? "inherit" : 0}
        right={isMobile ? "inherit" : 0}
        px={isMobile ? "inherit" : "4%"}
        mt={isMobile ? '5vh' : "inherit"}>
        <Select
          placeholder="Select Hash"
          isDisabled={!active}
          onChange={(e: ChangeEvent<HTMLSelectElement>) =>
            setSelectedHash(e.target.value)
          }
          focusBorderColor="gray.500"
        >
          {hashes.map((hash, i) => (
            <option key={i} value={hash.hash}>
              {`${hash.phrase} : ${hash.hash}`}
            </option>
          ))}
        </Select>
      </Box>
      <Box
        maxWidth={isMobile ? "inherit" : "128.9ch"}
        wordwrap="break-word"
        lineHeight={isMobile ? "inherit" : "14vh"}
        fontSize={isMobile ? { base: '2.8vw', md: '2.8vw' } : { base: '1vw', md: '1vw' }}
        visibility={selectedHash ? 'visible' : 'hidden'}
        py={isMobile ? '8%' : 'inherit'}>
        {activeRegex ? (
          <Highlighter
            // findChunks will take precedence over regex
            findChunks={highlightFunction
              ? ({ textToHighlight }) => highlightFunction(textToHighlight)
              : undefined}
            searchWords={[activeRegex]}
            textToHighlight={binaryData} />
        ) : (
          binaryData
        )}
      </Box>
    </Box>
  );
}
