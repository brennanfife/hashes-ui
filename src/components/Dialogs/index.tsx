import dynamic from 'next/dynamic';

const Wallets = dynamic(() => import('./Wallets'), { ssr: false });
const ViewHashes = dynamic(() => import('./ViewHashes'), { ssr: false });

export default function Dialogs() {
  return (
    <>
      <ViewHashes />
      <Wallets />
    </>
  );
}
