export const Maze = (hash) => (p5) => {
    var cols, rows;
    var w;
    var grid = [];
    var iterationNumber = 0;
    var cellsVisited = 0;

    var current;
    var stack = [];
    var destinationCell;
    var currentlyAnimating = false;

    var directionsFromHash;

    p5.setup = () => {
        p5.createCanvas(450, 450);

        const driftArray = hexToBase2(hash).split('').reduce((prev, curr) => {
            const x = prev[prev.length - 1];
            const newX = curr === '0' ? x - 1 : x + 1;
            return prev.concat([newX]);
        }, [0]);
        const maxDrift = Math.max(Math.abs(Math.min(...driftArray)), Math.max(...driftArray));

        w = 450 / maxDrift;
        cols = p5.round(p5.width / w);
        rows = p5.round(p5.height / w);
        p5.frameRate(20);

        directionsFromHash = hexToBase4(hash).split('');

        for (var j = 0; j < rows; j++) {
            for (var i = 0; i < cols; i++) {
                var cell = new Cell(i, j, p5);
                grid.push(cell);
            }
        }

        current = grid[0];
        stack.push(current);
        while (stack.length > 0) {
            current.visited = true;
            var next = current.checkNeighbors(false);
            if (next) {
                next.visited = true;
                stack.push(current);
                removeWalls(current, next);
                current = next;
            } else {
                current = stack.pop()
            }
        }

        grid[Number(BigInt(hash) % (BigInt(rows) * BigInt(cols)))].destinationCell = true;
        current = grid[0];
        p5.noLoop();
    }

    p5.draw = () => {
        p5.background(51);

        for (var i = 0; i < grid.length; i++) {
            grid[i].show();
        }

        if (current.destinationCell) {
            currentlyAnimating = false;
            p5.noLoop();
        }

        current.animationVisited = true;
        current.highlight();
        var next = current.getNextTraversableCell();
        if (next) {
            next.animationVisited = true;
            stack.push(current);
            current = next;
        } else if (stack.length > 0) {
            current = stack.pop();
        }
    }

    p5.mouseClicked = () => {
        if (currentlyAnimating) {
            return;
        }
        if (!(p5.mouseX <= p5.width && p5.mouseX >= 0 && p5.mouseY <= p5.height && p5.mouseY >= 0)) {
            return;
        }
        current = grid[0];
        stack = [];
        for (var i = 0; i < grid.length; i++) {
            grid[i].animationVisited = false;
        }
        currentlyAnimating = true;
        p5.loop();
    }

    function index(i, j) {
        if (i < 0 || j < 0 || i > cols - 1 || j > rows - 1) {
            return -1;
        }
        return i + j * cols;
    }

    function Cell(i, j, p5) {
        this.i = i;
        this.j = j;
        this.walls = [true, true, true, true];
        this.visited = false;
        this.animationVisited = false;
        this.destinationCell = false;

        this.getNextTraversableCell = function () {
            var neighbors = [];

            var top = grid[index(i, j - 1)];
            var right = grid[index(i + 1, j)];
            var bottom = grid[index(i, j + 1)];
            var left = grid[index(i - 1, j)];

            if (top && !top.animationVisited && !top.walls[2]) {
                neighbors.push(top);
            }

            if (bottom && !bottom.animationVisited && !bottom.walls[0]) {
                neighbors.push(bottom);
            }

            if (left && !left.animationVisited && !left.walls[1]) {
                neighbors.push(left);
            }

            if (right && !right.animationVisited && !right.walls[3]) {
                neighbors.push(right);
            }

            if (neighbors.length > 0) {
                return neighbors[neighbors.length - 1];
            }

            return undefined;
        }

        this.checkNeighbors = function (isAnimation) {
            var neighbors = [];

            var top = grid[index(i, j - 1)];
            var right = grid[index(i + 1, j)];
            var bottom = grid[index(i, j + 1)];
            var left = grid[index(i - 1, j)];

            if (top && !top[isAnimation ? 'animationVisited' : 'visited']) {
                neighbors.push(top);
            }
            if (bottom && !bottom[isAnimation ? 'animationVisited' : 'visited']) {
                neighbors.push(bottom);
            }
            if (right && !right[isAnimation ? 'animationVisited' : 'visited']) {
                neighbors.push(right);
            }
            if (left && !left[isAnimation ? 'animationVisited' : 'visited']) {
                neighbors.push(left);
            }

            if (neighbors.length > 0) {
                var nextDirectionFromHash = directionsFromHash[(iterationNumber++ % directionsFromHash.length + directionsFromHash.length) % directionsFromHash.length];
                return neighbors[nextDirectionFromHash % neighbors.length];
            } else {
                return undefined;
            }
        }

        this.highlight = function () {
            var x = this.i * w;
            var y = this.j * w;
            p5.noStroke();
            p5.fill(255, 255, 255, 100);
            p5.rect(x, y, w, w);
        }

        this.show = function () {
            var x = this.i * w;
            var y = this.j * w;
            p5.stroke(255);

            if (this.walls[0]) {
                p5.line(x, y, x + w, y); // top or bottom
            }
            if (this.walls[1]) {
                p5.line(x + w, y, x + w, y + w); // left or right
            }
            if (this.walls[2]) {
                p5.line(x + w, y + w, x, y + w); // top or bottom
            }
            if (this.walls[3]) {
                p5.line(x, y + w, x, y); // left or right
            }

            if (this.visited) {
                p5.noStroke();
                p5.fill(0, 0, 255, 100);
                p5.rect(x, y, w, w);
            }

            if (this.destinationCell) {
                p5.fill(0, 255, 0, 100);
                p5.rect(x, y, w, w);
            }
        }
    }


    function removeWalls(a, b) {
        var x = a.i - b.i;
        if (x === 1) {
            a.walls[3] = false;
            b.walls[1] = false;
        } else if (x === -1) {
            a.walls[1] = false;
            b.walls[3] = false;
        }

        var y = a.j - b.j;
        if (y === 1) {
            a.walls[0] = false;
            b.walls[2] = false;
        } else if (y === -1) {
            a.walls[2] = false;
            b.walls[0] = false;
        }

    }

    function hexToBase4(hex) {
        return hex
            .replace('0x', '')
            .split('')
            .map((i) => parseInt(i, 16).toString(4).padStart(2, '0'))
            .join('');
    }

    function hexToBase2(hex) {
        return hex
            .replace('0x', '')
            .split('')
            .map((i) => parseInt(i, 16).toString(2).padStart(4, '0'))
            .join('');
    }
}