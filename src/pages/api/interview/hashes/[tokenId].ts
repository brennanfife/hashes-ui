const tokens = [
    {
        tokenId: 0,
        address: "0xe52e6065d32e02f3bac4bdc4c45564c3a01af09e",
        hash: "0x7d035dc5c256b98b42d7dfbc2be4447436bdca3c2783e1d5f5ee497881ed66c3",
        isDAO: false
    },
    {
        tokenId: 1,
        address: "0x33e678983d2e28e1ffb893998c9c0dc8c5a334ba",
        hash: "0x7e58266ecffa09aeea28ef1d6b24b0025458f8ea00585c28f2fcbea1624be162",
        isDAO: false
    },
    {
        tokenId: 2,
        address: "0xf530c29b3a358a03c047e5e3f9b4f6bce216ea75",
        hash: "0x7534e5791e062e1c8d1a760ce58a3ffa599dcb75b266b7e9faa88aa3f3ab90ae",
        isDAO: false
    },
    {
        tokenId: 3,
        address: "0xabb23456dee07aa3abb0d4c2c0b33cdfeace8d64",
        hash: "0x71ea0ae63475b4474c6cf059a5d08263fdd867dc956737439222f569d0803538"
    },
    {
        tokenId: 4,
        address: "0x72e0797804b83a42569b58d44b3de1495401db1f",
        hash: "0xba01ab6d8919e6092f0fcbedecac153795be4018c8c0247511bfa5b9b18ec508"
    },
    {
        tokenId: 5,
        address: "0x251d094b64fbde47e99d7a5cf07f90762af8e773",
        hash: "0x50d640618d483624317fcec47ec25a83df11cf465fe5006b2ed55410b8826662"
    },
    {
        tokenId: 6,
        address: "0xc60ffafe57e6e2498116184040736deb37040deb",
        hash: "0x8a3ba62932c820057ad3c628836c1a1867fa8eebd7e58a1f3fca519ffcda0316"
    },
    {
        tokenId: 7,
        address: "0x8b97964f22e45ba7ecfebc2880355ae9128ac4d2",
        hash: "0x755cb305e61c52943edf9812bceb379db0520dc71dd4ee9bfbf480c15234b993"
    },
    {
        tokenId: 8,
        address: "0x0dbbdb818c13cb028bd8eec900904d7d872954a0",
        hash: "0xe1f772ca93e6556b72a303e3b226b2adc8f76359dd332501fd8308c8460adda2"
    },
    {
        tokenId: 9,
        address: "0xff38c4657929f44c8f2704e91c2960fe89ca8075",
        hash: "0xbf2096f0f57435a9192b6c86b582bab505866aeafef96f66ba20fd3a180b4344"
    },
    {
        tokenId: 10,
        address: "0x9737d3047ce8520da3efa06d17af5863c391b8a2",
        hash: "0x0da9673e1389f3db2cba308a39aa2fa4acf06a1e27cbb1c595ddfea468664477"
    },
    {
        tokenId: 11,
        address: "0xbe49465cac70b03e8324e7bed9b102e9ad10610e",
        hash: "0x04007e5f6440eada8fce20321ff52dff5095484438f1d53319a421a7729c5c25"
    },
    {
        tokenId: 12,
        address: "0x43cfa0db5f53f7181aec1a2a4e4488d451f51d69",
        hash: "0xc029ba6c47c00fe4abc17099d3114c012755b6ca1d173cd5d2ef8a1cef6880e1",
    },
    {
        tokenId: 13,
        address: "0xccb8473a0217f20ce5824642110c58d8d12022e8",
        hash: "0x6f449909aa777d6a5daf4299c5f7ceac3b5e79b4dab7d25ae06d528eb7b7d5a2"
    },
    {
        tokenId: 14,
        address: "0x1bad8af9b73b5e2f458b4a901825720942cbe8be",
        hash: "0x81ad2ee1b7cd17af4d2f7a5c1e461b859bcaf887ed6d6adb6119d3dd2137716d"
    },
    {
        tokenId: 15,
        address: "0x81081f418be6df2449fa4c32cd1d5cfef5e7073f",
        hash: "0xb0108f8e698cbac5e20422b5539459956767083422c19813c57b0e2bbe55eaab"
    },
    {
        tokenId: 16,
        address: "0x22b09095ce8e34f2a862011d04b4b0ce3b1fcf28",
        hash: "0x9b7181403e7a517c67741a12c492adfaaa1e689f863be4b9be749032da8320f1"
    },
    {
        tokenId: 17,
        address: "0x43f1bd0616be48e10a0ffd0ba9c7e9e6144459e6",
        hash: "0x4c15820f03e9f7b359579bf0f4c1c0e3e2fbd66cf55552437d74583dd51a2ee3"
    },
];

export default async function handler(req, res) {
    const { tokenId } = req.query;

    const tokenIdAsNum = parseInt(tokenId);

    if ( tokenIdAsNum < 0 || tokenIdAsNum > 17) {
        res.status(404).send();
        return;
    }

    res.status(200).json(tokens[tokenIdAsNum]);
}