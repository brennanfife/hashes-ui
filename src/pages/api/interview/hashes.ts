export default async function handler(req, res) {
    res.status(200).json({
        name: "Hashes",
        symbol: "HASH",
        address: "0xdbf477d656e7d1a2fe9cdf6a20a3da5e01e3dd77",
        tokenOwners: {
            0: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            1: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            2: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            3: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            4: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            5: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            6: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            7: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            8: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            9: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            10: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            11: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            12: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            13: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            14: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            15: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            16: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            17: "0xf4f949cb76929600d47019975201a5b7b5dc57e6"
        }
    });
}