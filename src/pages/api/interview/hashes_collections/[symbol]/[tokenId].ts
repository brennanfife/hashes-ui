const tokens = [
    {
        tokenId: 0,
        hashTokenIdUsedToMint: 3,
        address: "0xe4f91b23417e82d30c88bc9efb7a410a96855135",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455029755-OXISSW9FL819QCZH7LLB/tyler-hobbs-fidenza-635.png?format=750w"
    },
    {
        tokenId: 1,
        hashTokenIdUsedToMint: 4,
        address: "0x9089420f96c522e39c24d069b4c20b53e8f38fb7",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455147380-IJOZS9S3M8NTWT9KD4JC/tyler-hobbs-fidenza-768.png?format=750w"
    },
    {
        tokenId: 2,
        hashTokenIdUsedToMint: 5,
        address: "0x3194e3dc5f57804b3bfd5160f99af7539ff6d0bc",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455302298-XCJ7KFS5QP2CC546A1BU/tyler-hobbs-fidenza-26.png?format=750w"
    },
    {
        tokenId: 3,
        hashTokenIdUsedToMint: 0,
        address: "0x300950f8c4b771a869cbf7c8f9722883510e6dd7",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455383420-8C3CQ9BJ8W3XFIBHHILH/tyler-hobbs-baked-78.png?format=750w"
    },
    {
        tokenId: 4,
        hashTokenIdUsedToMint: 10,
        address: "0xbeb3033390466d73de36c2cd88423b6db59db668",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455469476-IPC2108QZACV9R70D843/tyler-hobbs-fidenza-325.png?format=750w"
    },
    {
        tokenId: 5,
        hashTokenIdUsedToMint: 1,
        address: "0x2d3f85cff48c1f71b15f0c2d8075c0d6a23e5882",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623455598632-A82ME30I9Y3DF0AG8BKS/tyler-hobbs-fidenza-720.png?format=750w"
    },
    {
        tokenId: 6,
        hashTokenIdUsedToMint: 11,
        address: "0x9e514ca8df2a8caf0b3553ccc2f74327a71fc623",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1630359211575-9YGZZC3Z6UKRPM0X2H5M/tyler-hobbs-fidenza-680.png?format=750w"
    },
    {
        tokenId: 7,
        hashTokenIdUsedToMint: 2,
        address: "0x2c10c2c97ca14106eae8290ae3b770b806adb3c5",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456028313-TEPTRXBPDWGF7DK74LTT/tyler-hobbs-fidenza-607.png?format=750w"
    },
    {
        tokenId: 8,
        hashTokenIdUsedToMint: 9,
        address: "0xbcbf2c901432f50f153d1be0b2da92770426ee61",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456120067-K6RS7X42KO8WABH0WNFT/tyler-hobbs-fidenza-738.png?format=750w"
    },
    {
        tokenId: 9,
        hashTokenIdUsedToMint: 12,
        address: "0x7db6637d7e5c103b04b5c4834835471552bc6eb4",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456197457-05XW2UYXZ56RM89ZY3HL/tyler-hobbs-fidenza-237.png?format=750w"
    },
    {
        tokenId: 10,
        hashTokenIdUsedToMint: 13,
        address: "0x8724b61ea3716b516770e7242dba80cb913d209a",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456525896-93CS1DU00M2AH1C2ZVG4/tyler-hobbs-fidenza-247.png?format=750w"
    },
    {
        tokenId: 11,
        hashTokenIdUsedToMint: 8,
        address: "0x0a30d9e48a566eaa136e9eaf539ef2c81c298c6e",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456610197-OPNCEIZ7HW45OCRN8D59/tyler-hobbs-fidenza-229.png?format=750w"
    },
    {
        tokenId: 12,
        hashTokenIdUsedToMint: 7,
        address: "0x86c413649f568fff083d8a0836cb2d86c8bda7d4",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623456694896-C6QUP977S8I633VNA3T3/tyler-hobbs-fidenza-531.png?format=750w"
    },
    {
        tokenId: 13,
        hashTokenIdUsedToMint: 6,
        address: "0xb4eafb98c04eb54716f90ca9c5afa28f51fe9210",
        image: "https://images.squarespace-cdn.com/content/v1/5c12933f365f02733c923e4e/1623453908048-VIU8SP6XDI8P0TQEM5YG/tyler-hobbs-fidenza-430.png?format=1000w"
    },
];

export default async function handler(req, res) {
    const { tokenId } = req.query;

    const tokenIdAsNum = parseInt(tokenId);

    if ( tokenIdAsNum < 0 || tokenIdAsNum > 13) {
        res.status(404).send();
        return;
    }

    res.status(200).json(tokens[tokenIdAsNum]);
}