export default async function handler(req, res) {
    const { tokenId } = req.query;

    const tokenIdAsNum = parseInt(tokenId);

    res.status(200).json(tokenIdAsNum % 4 === 0 ? '100000000000000000' : '200000000000000000');
}