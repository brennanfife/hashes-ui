export default async function handler(req, res) {
    const { symbol } = req.query;

    if (symbol !== 'PERM2') {
        res.status(404).send();
        return;
    }

    res.status(200).json({
        name: "Permutation 2",
        symbol: "PERM2",
        address: "0xbdab86a0aa0e66b3a679bdf7118bd736e9f62367",
        tokenOwners: {
            0: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            1: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            2: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            3: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            4: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            5: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            6: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            7: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            8: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            9: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            10: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
            11: "0xf4f949cb76929600d47019975201a5b7b5dc57e6",
            12: "0xbbd3f13c8683102203aa03dc695e6ac53a578354",
            13: "0x3324ac689bcd5c6db75c1f274f172a72f6d20699",
        },
    });
}