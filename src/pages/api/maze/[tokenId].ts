import { ethers } from 'ethers';

import { INFURA_PREFIXES } from '../../../config/connectors';
import HASHES_ABI from '../../../Hashes.json';
import HASHES_COLLECTION_ERC721_CLONEABLE_V1 from '../../../HashesCollectionERC721CloneableV1.json';
import { HASHES_ADDRESS, MAZE_ADDRESS } from '../../../utils';

function getHashesContract(chain_id: string | undefined) {
    // TODO: Fix this - this is currently set to rinkeby only for testing purposes
    const chainId = 4;
    const provider = new ethers.providers.InfuraProvider(INFURA_PREFIXES[chainId], process.env.API_INFURA_PROJECT_ID);
    return new ethers.Contract(HASHES_ADDRESS[chainId], HASHES_ABI.abi, provider);
}

function getMazeContract(chain_id: string | undefined) {
    // TODO: Fix this - this is currently set to rinkeby only for testing purposes
    const chainId = 4;
    const provider = new ethers.providers.InfuraProvider(INFURA_PREFIXES[chainId], process.env.API_INFURA_PROJECT_ID);
    return new ethers.Contract(MAZE_ADDRESS[chainId], HASHES_COLLECTION_ERC721_CLONEABLE_V1.abi, provider);
}

export default async function handler(req, res) {
    const { tokenId, chain_id } = req.query;

    if (isNaN(Number(tokenId))) {
        res.status(400).send();
        return;
    }

    const mazeContract = getMazeContract(chain_id);
    const hashesContract = getHashesContract(chain_id);

    const [exists, hashesTokenId] = await mazeContract.collectionTokenIdToHashesIdMapping(tokenId);

    if (!exists) {
        res.status(404).send();
        return;
    }

    const hashUsedToGenerateMaze = await hashesContract.getHash(hashesTokenId);

    res.status(200).json({
        name: `Maze ID #${tokenId}`,
        description: 'TODO',
        animation_url: `https://www.thehashes.xyz/hashes_collections/maze/maze.html?hash=${hashUsedToGenerateMaze}`,
        attributes: [
            {
                trait_type: "Generative Hash",
                value: hashUsedToGenerateMaze
            }
        ]
    });
}