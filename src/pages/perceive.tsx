import {
  Box,
  chakra,
  Flex,
  Heading,
  ListItem,
  Text,
  UnorderedList,
  useColorModeValue as mode,
} from '@chakra-ui/react';
import { useState } from 'react';

import {
  BinaryTreeBranch,
  getBinaryTreeData,
} from '../components/Visualize/BinaryTreeBranch';
import HashBinarySelector from '../components/Visualize/HashBinarySelector';
import { useSelectedHash } from '../context';
import { useDeviceDetect } from '../hooks';
import { hex2bin } from '../utils';
import { HASH_ATTRIBUTES } from '../utils/hash_attributes';

export default function Perceive() {
  const [selectedHash] = useSelectedHash();
  const [activeStat, setActiveStat] = useState<null | ActiveStat>(null);
  const { isMobile } = useDeviceDetect();

  const RegexStat = ({
    descriptionShort,
    description,
    regex,
    highlightFunction,
  }: ActiveStat & { descriptionShort: string }) => {
    const isActive =
      activeStat && activeStat.regex && regex
        ? activeStat.regex.toString() === regex.toString()
        : false;
    const match = regex ? hex2bin(selectedHash).match(regex) : null;
    return (
      <ListItem
        fontWeight={isActive ? 'bold' : 'normal'}
        onMouseEnter={() =>
          setActiveStat({
            regex: regex,
            description: description,
            highlightFunction: highlightFunction,
          })
        }
        onMouseLeave={() => setActiveStat(null)}
      >
        {`${descriptionShort}: ${match !== null ? match[0].length : 0}`}
      </ListItem>
    );
  };

  const ValueStat = ({
    name,
    descriptionShort,
    description,
    value,
  }: ActiveStat & { descriptionShort: string; value: any }) => {
    const isActive = activeStat && activeStat.name === name;
    return (
      <ListItem
        fontWeight={isActive ? 'bold' : 'normal'}
        onMouseEnter={() =>
          setActiveStat({ name: name, description: description })
        }
        onMouseLeave={() => setActiveStat(null)}
      >
        {`${descriptionShort}: ${value}`}
      </ListItem>
    );
  };

  const binaryTreeData = getBinaryTreeData(selectedHash);
  const isActiveTreeRelatedStat =
    activeStat?.name === 'BinaryTreeBranch' ||
    activeStat?.name === 'MaxDriftFromCenter';

  return (
    <Box
      as="section"
      mx="auto"
      maxW={{ base: 'xl', md: '7xl' }}
      p={{ base: '6', md: '12' }}
    >
      <Flex justify="center" direction="column">
        <Box>
          <Heading
            // size="lg"
            // size={{ base: 'sm', md: 'lg' }}
            fontWeight="medium"
            color={mode('gray.600', 'gray.300')}
          >
            Explore <chakra.span color="red.500">patterns</chakra.span> in your
            hash data
          </Heading>
        </Box>
        <Box align="center">
          <HashBinarySelector
            activeRegex={activeStat?.regex}
            highlightFunction={activeStat?.highlightFunction}
          />
        </Box>
        <Flex
          justifyContent="space-between"
          visibility={selectedHash ? 'visible' : 'hidden'}
        >
          <Box flex="2">
            <Flex direction="column">
              <Flex
                direction="row"
                justifyContent="space-between"
                height="120px"
              >
                <Box>
                  <Heading size="md" color={mode('gray.600', 'gray.300')}>
                    Stats
                  </Heading>
                  {activeStat === null ? (
                    <Text
                      py="4"
                      fontSize={{ base: '0.8rem', md: '1.2rem' }}
                      color={mode('gray.600', 'inherit')}
                    >
                      {isMobile ? 'Click' : 'Hover over'} items for description.
                    </Text>
                  ) : (
                    <Text
                      py="4"
                      fontSize={{ base: '0.8rem', md: '1.2rem' }}
                      color={mode('gray.600', 'inherit')}
                    >
                      {`${activeStat!.description}`}
                    </Text>
                  )}
                </Box>
              </Flex>
              <UnorderedList mt="8" spacing="3">
                {HASH_ATTRIBUTES.map((attribute, i) => {
                  if (attribute.regex) {
                    return <RegexStat key={i} {...attribute} />;
                  }
                  const value = attribute.calculationFunction
                    ? attribute.calculationFunction(selectedHash)
                    : null;
                  return <ValueStat key={i} {...attribute} value={value} />;
                })}
              </UnorderedList>
            </Flex>
          </Box>
          <Box flex="1">
            {selectedHash ? (
              <Box
                onMouseEnter={() =>
                  setActiveStat({
                    name: 'BinaryTreeBranch',
                    description:
                      'A tree branch representation of the number in binary form. Branch left for each 0 and right for each 1.',
                  })
                }
                onMouseLeave={() => setActiveStat(null)}
              >
                <Text
                  fontSize="md"
                  fontWeight={isActiveTreeRelatedStat ? 'bold' : 'normal'}
                  color={mode('gray.600', 'inherit')}
                  textAlign="center"
                  paddingBottom="4"
                >
                  Unique binary tree branch
                </Text>
                <BinaryTreeBranch
                  binaryTreeData={binaryTreeData}
                  isHighlighted={isActiveTreeRelatedStat}
                />
              </Box>
            ) : null}
          </Box>
        </Flex>
      </Flex>
    </Box>
  );
}
