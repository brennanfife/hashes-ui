import { Box, Divider, Heading, Text } from '@chakra-ui/layout';

export default function About() {
  return (
    <Box
      mx="auto"
      w="100%"
      maxW={{ base: 'xl', md: '7xl' }}
      px={{ base: '6', md: '8' }}
    >
      <Box my="3rem">
        <Heading
          fontSize="2.5rem"
          fontWeight="500"
          mb=".5rem"
          mt="0"
          textAlign={{ base: 'left', md: 'center' }}
        >
          About Hashes
        </Heading>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          What is Hashes?
        </Heading>
        <Text mt="0" mb="1rem">
          Hashes is a new NFT building block and a DAO in one. A Hash is an NFT,
          a distribution mechanism, a source of entropy, and a vote in the DAO.
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          How can I join the DAO?
        </Heading>
        <Text mt="0" mb="1rem">
          The first 1000 Hashes NFT cost 1 ETH to mint, and after that the DAO
          decides if there will be a minting fee. Only the first 1000 Hashes are
          Hashes DAO members, able to make proposals and vote. Voting rights are
          always associated to these first thousand hashes - if you transfer one
          of these tokens, you are transferring your voting rights as well.
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          What does the DAO do?
        </Heading>
        <Text mt="0" mb="1rem">
          The DAO can commission projects (such as artwork), either unique to be
          owned by the DAO, or to be generated and distributed via Hashes. The
          DAO can control the treasury and set minting fees for future Hashes.
          The DAO cannot adjust supply, or transfer token ownership between
          users.
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          How can I mint a Hash?
        </Heading>
        <Text mt="0" mb="1rem">
          You can use this app to mint your first Hash by inputting a string of
          your choosing. This string will always be associated with your Hash.
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          What can I do with a Hash as a token holder?
        </Heading>
        <br />
        <Text mt="0" mb="1rem">
          Right now, think about the Hash like some raw material that has
          properties, but we may not have discovered them all yet. Check out the
          “perceive” tab to see some “intrinsic” traits that are found in
          Hashes, like drift, leading 0’s and more. You can participate in an
          infinitely-growing set of integrations using your Hash.
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          What can I do with a Hash as a content creator?
        </Heading>
        <Text mt="0" mb="1rem">
          Artists, game designers, and others can use Hashes as the source of
          randomness in generative work or bridge together to form an identity.
          A content creator is now empowered to define their distribution
          channel however they see fit (e.g. artwork dropped to only the first
          1K hashes, or even-numbered hashes, etc.).
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          A Hash seems kind of simple.
        </Heading>
        <Text mt="0" mb="1rem">
          Yes. That’s the whole point!
        </Text>
        <br />
        <Heading fontSize="1.25rem" fontWeight="500" mb=".5rem" mt="0">
          Why are you doing this?
        </Heading>
        <Text mt="0" mb="1rem">
          Because it’s fun! NFTs, and DAOs, are all experiments in coordination,
          pushing the boundaries of what creates community and what communities
          can accomplish. Hashes and their pseudo-randomness are a building
          block of the decentralized web. What if you could own your own random
          seed? What new dynamics emerge?
        </Text>
      </Box>
    </Box>
  );
}
