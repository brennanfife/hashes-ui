import {
    Box,
    Flex,
    Heading,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Text,
    useColorModeValue as mode,
} from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { ethers } from 'ethers';
import { useEffect, useRef, useState } from 'react';

import HASHES_ABI from '../Hashes.json';
import { useContract } from '../hooks';
import { Maze } from '../playground/maze';
import { HASHES_ADDRESS } from '../utils';
let p5, p5Instance;

export default function Playground() {
    const { chainId } = useWeb3React();
    const HashesContract = useContract(
        chainId && HASHES_ADDRESS[chainId],
        HASHES_ABI.abi,
        true,
    );
    const [hash, setHash] = useState<string | undefined>();
    const [tokenId, setTokenId] = useState<string | undefined>();
    const p5Ref = useRef(document.createElement("div"));

    useEffect(() => {
        p5 = require('p5');
    }, []);

    useEffect(() => {
        if (!p5 || !hash) {
            return;
        }
        if (p5Instance) {
            p5Instance.remove();
        }
        if (p5Ref.current.firstChild) {
            p5Ref.current.removeChild(p5Ref.current.firstChild);
        }
        p5Instance = new p5(Maze(hash), p5Ref.current);
    }, [hash]);

    useEffect(() => {
        (async () => {
            if (tokenId === undefined || tokenId === '') {
                setHash(undefined);
                return;
            }
            if (HashesContract) {
                try {
                    const hash = await HashesContract.getHash(tokenId);
                    if (hash === ethers.constants.HashZero) {
                        setHash(undefined);
                    } else {
                        setHash(hash);
                    }
                } catch (error: any) {
                    console.error(error);
                }
            }
        })();
    }, [HashesContract, tokenId]);

    return (
        <Box
            as="section"
            mx="auto"
            maxW={{ base: 'xl', md: '7xl' }}
            p={{ base: '6', md: '12' }}
        >
            <Flex justify="center" direction="column">
                <Box>
                    <Heading
                        fontWeight="medium"
                        color={mode('gray.600', 'gray.300')}
                    >
                        Hashes Playground
                    </Heading>
                </Box>
                <NumberInput mt="8" mb="8" onChange={(val) => {
                    setTokenId(val);
                }}>
                    <NumberInputField placeholder="Enter hashes token ID" />
                    <NumberInputStepper>
                        <NumberIncrementStepper />
                        <NumberDecrementStepper />
                    </NumberInputStepper>
                </NumberInput>
                <Text
                    fontSize={{ base: '0.8rem', md: '1.2rem' }}
                    color={mode('gray.600', 'inherit')}
                    mb="8"
                >
                    {hash ? `Hash: ${hash}` : `Enter a valid token Id to retrieve hash`}
                </Text>
                <div ref={p5Ref}></div>
            </Flex>
        </Box >
    );
}