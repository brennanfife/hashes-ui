export const MetaMaskErrors = {
  userRejectsTxSignature:
    'MetaMask Tx Signature: User denied transaction signature.',
  userRejectsMessageSignature:
    'MetaMask Message Signature: User denied message signature.',
};
